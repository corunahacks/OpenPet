<?php
namespace App\DataFixtures;

use App\Entity\Location;
use App\Entity\Province;
use App\Entity\Race;
use App\Entity\Specie;
use App\Entity\Specimen;
use App\Entity\Origin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 races! Bam!
        for ($i = 0; $i < 20; $i++) {
            $race = new Race();
            $race->setName("Race ".$i);
            $manager->persist($race);
        }

        for ($i = 0; $i < 5; $i++) {
            $specie = new Specie();
            $specie->setName("Specie ".$i);

            $manager->persist($specie);
        }


        for ($i = 0; $i < 50; $i++) {
            $province = new Province();
            $province->setName("Province ".$i);

            $manager->persist($province);
            $manager->flush();

        }

        $this->addReference('province', $province);


        for ($i = 0; $i < 3; $i++) {
            $origin = new Origin();
            $origin->setName("Origin ".$i);

            $manager->persist($origin);
        }

        $manager->flush();
        $this->addReference('origin', $origin);


        for ($i = 0; $i < 50; $i++) {
            $location = new Location();
            $location->setName("Location ".$i);
            $location->setProvince($this->getReference("province"));

            $manager->persist($location);
        }

        $manager->flush();
        $this->addReference('location', $location);



        for ($i = 0; $i < 100; $i++) {
            $specimen = new Specimen();
            $specimen->setName("Specimen ".$i);
            $specimen->setLocation($this->getReference("location"));
            $specimen->setOrigin($this->getReference("origin"));


            $manager->persist($location);
        }

        $manager->flush();


    }
}