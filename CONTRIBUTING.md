# Contributing

We love pull requests from everyone.



Fork, then clone the repo:

    git clone git@gitlab.com:corunahacks/OpenPet.git

Set up your machine:

    ./bin/setup

Push to your fork and [submit a pull request][pr].

[pr]: https://gitlab.com/corunahacks/OpenPet/compare

At this point you're waiting on us. We like to at least comment on pull requests
within three business days (and, typically, one business day). We may suggest
some changes or improvements or alternatives.

Some things that will increase the chance that your pull request is accepted:

* Write tests.
* Write a [good commit message][commit].

[commit]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

The source code is provided "AS IS" with no warranties, and confers no rights. Use of included source code
samples are subject to the terms specified on the [License](https://gitlab.com/corunahacks/OpenPet/blob/master/LICENSE)


#Installation infraestructure

Install docker and docker compose:

https://get.docker.com

Install docker-compose:

https://docs.docker.com/compose/install/#install-compose


Execute:

Go to openpet dir

> cd openpet

Modify your env file in openpet/api/.env

> nano openpet/api/.env

```
# This file is a "template" of which env vars need to be defined for your application
# Create environment variables when deploying to production
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=!ChangeMe!
TRUSTED_PROXIES=10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
TRUSTED_HOSTS=localhost,api
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=pgsql://api-platform:!ChangeMe!@db/api
###< doctrine/doctrine-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://localhost:?[0-9]*$
###< nelmio/cors-bundle ###

VARNISH_URL=http://cache-proxy

```


Initialize the proyect

> docker-compose up -d

# Usefull commands

Update schema:

>  docker-compose exec php bin/console doctrine:schema:update --force


Load sample data:

> docker-compose exec php bin/console doctrine:fixtures:load

Update dependencies 

> docker-compose exec php composer install --prefer-dist