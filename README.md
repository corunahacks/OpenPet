This project is a fork of: https://github.com/gpul-labs/OpenPet/

# ![logo](https://github.com/gpul-labs/OpenPet/blob/master/logo.png?raw=true)
### Coruña Hacks

This project aims to create an open data source for pet adoption. Emphasizing the idea of unifying the data obtained, both from local animal shelters and from other sources through scraping, and exposing the result through a public API for its use.
